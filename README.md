#   [Foliant](https://github.com/foliant-docs/foliant). English Documentaion.

The following sections are currently translated and revised:

- About Foliant
- Installation
- Quick Start
- Changelog

In progress:

- How to...
- Backends

Recently updated:

- Troubleshooting

The following sources were used in this project:

Original Foliant documentaion (english): [https://foliant-docs.github.io/docs/](https://foliant-docs.github.io/docs/);

"Foliant Fast Intro" guide (russian): [https://norrskog.gitlab.io/foliant-fast-intro/](https://norrskog.gitlab.io/foliant-fast-intro/)
