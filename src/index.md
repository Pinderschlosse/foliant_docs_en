## About Foliant

**Foliant** is a universal documentation development tool based on the 'Docs as Code' approach. It allows you to create websites and offline documents (pdf and docx) from a single Markdown source.

**Foliant** is a high-level tool, i.e. it uses other programs for its work. For example, [Pandoc](https://pandoc.org/) is used to generate pdf and docx, and [MkDocs](https://www.mkdocs.org/) - for websites.

**Foliant** [preprocessors](https://foliant-docs.github.io/docs/preprocessors/general_notes/) allow you to include parts of some documents into others, show and hide contents with flags, display charts from text, and much more.

In other words, being actually a stand-alone product, **Foliant** is a link for other tools due to its many opensource dependencies.

## Who Is Foliant Created For?

**Foliant** is created by technical communicators for technical communicators. It can also be useful if:

- you need to send the documentation as pdf, docx or website;
- you want to use Markdown with a consistent extension system instead of custom syntax for each new bit of functionality;
- you love [reStructuredText’s](https://ru.wikipedia.org/wiki/ReStructuredText) extensibility and [Asciidoc’s](http://asciidoc.org/) flexibility, but still prefer to use Markdown;
- you need a tool for which you can write your own extension without having to deal with something like [Sphinx](https://github.com/mazhartsev/sphinx-ru);
- your documentation is distributed across many repositories, and you want to reuse these parts between documents.

[Here](https://www.youtube.com/watch?v=mJ5IqzmXuMo) you can see our presentation (in Russian) to find out more about Foliant.

**NB:** At the moment, the documentation for Foliant is in the refactoring stage. If during the installation or use you have any difficulties - please refer to the **[Troubleshooting](https://pinderschlosse.gitlab.io/foliant_docs_en/troubleshooting/)** section or ask your question in our Telegram-chat: [https://t.me/foliantdocs](https://t.me/foliantdocs).
