# Quick Start

## Create a New Project

<pre style="background-color: #b7def5"><span>⚠</span> Please note that Docker uses its own commands.</pre>

To create a new Foliant project, enter:

    $ foliant init

If you are using Docker, enter:

    $ docker run --rm -it -v `pwd`:/usr/app/src -w /usr/app/src foliant/foliant init

Then enter the project name (for example, Hello Foliant):

    Enter the project name: Hello Foliant

As a result, you will receive the following message:

     ✔ Generating Foliant project
    ─────────────────────
    Project "Hello Foliant" created in hello-foliant

Verify that the command executed correctly. Enter:

    $ cd hello-foliant
    $ tree

<pre style="background-color: LemonChiffon"><span>⚠</span><strong> For Ubuntu:</strong> if you enter <samp style="color: SteelBlue">tree</samp> and receive a <samp style="color: DarkRed">command 'tree' not found</samp> error,  
then enter <samp style="color: SteelBlue">sudo apt install tree</samp> command and retry the request.</pre>

As a result, you will see a local 'hello-foliant' folder containing the components of your project:

    ├── docker-compose.yml
    ├── Dockerfile
    ├── foliant.yml
    ├── README.md
    ├── requirements.txt
    └── src
        └── index.md

where:

`foliant.yml` - project's configuration file,

`src` - directory containing the source content of the project. Contains `index.md` file by default,

`requirements.txt` - list of Python packages required for the project: backends and preprocessors, themes for MkDocs, etc.,

`Dockerfile` and `docker-compose.yml` - Files needed to create a project in Docker..

## Create and Run a Local Site

In the project folder, run the following command:

    $ foliant make site

If you are using Docker, enter:

    $ docker-compose run --rm hello-foliant make site

As a result, you will receive the following message:

    ✔ Parsing config
    ✔ Applying preprocessor mkdocs
    ✔ Making site with MkDocs
    ─────────────────────
    Result: Hello_Foliant-<YYYY-MM-DD>.mkdocs

where:

`Hello_Foliant` - name of the project,

`<YYYY-MM-DD>` - current date (generated automatically).

The site is ready. To view it, go to the folder and run the local HTTP server startup command:

    $ cd Hello_Foliant-<YYYY-MM-DD>.mkdocs
    $ python3 -m http.server

You will receive a message:

    Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...

The local HTTP server is running. Following the link [http://0.0.0.0:8000/](http://0.0.0.0:8000/), you will see your website.

![Landing page](https://pinderschlosse.gitlab.io/foliant_docs_en/images/basic-mkdocs.png)

To stop the site while in the terminal, press Ctrl+C

## Create PDF

> <span>⚠</span> To create a PDF, make sure you have Pandoc and TeXLive installed (see ["Installation"](https://pinderschlosse.gitlab.io/foliant_docs_en/installation/) section)

While in the project directory, run:

    $ foliant make pdf

If you are using Docker, then:

*   uncomment `foliant/foliant:pandoc` line in `Dockerfile` file of your project. As a result, you should get this:

        # FROM foliant/foliant
        # If you plan to bake PDFs, uncomment this line and comment the line above:
        FROM foliant/foliant:pandoc

        COPY requirements.txt .

        RUN pip3 install -r requirements.txt

    > If you previously ran `docker-compose run` with the old image, run `docker-compose build` to rebuild the image. In addition, you can launch it whenever you need to update your version of the required packages from the `requirements.txt`.

*   Next, run the command in the project directory:

        $ docker-compose run --rm hello-foliant make pdf

You will receive the following message:

    ✔ Parsing config
    ✔ Applying preprocessor flatten
    ✔ Making pdf with Pandoc
    ─────────────────────
    Result: Hello_Foliant-<YYYY-MM-DD>.pdf 

where:

`Hello_Foliant` - name of the project,

`<YYYY-MM-DD>` - current date (generated automatically).

Your PDF is ready! It will look something like this:

![PDF](https://pinderschlosse.gitlab.io/foliant_docs_en/images/basic-pdf.png)
