# Troubleshooting

---

#### When you install Ubuntu, an error occurs (13: Permission denied) :

    E: Could not open lock file /var/lib/dpkg/lock-frontend - open (13: Permission denied)
    E: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), are you root?

This error occurs if another installer is still running or if the previous installer did not complete correctly. There are 3 possible solutions:

**Option 1**

Execute the command:

    sudo killall apt apt-get

**All** `apt` and `apt-get` processes will be completed at once.

**Option 2**

Execute the command:

    ps aux | grep -i apt

You will receive a similar message:

    root 5183 0.0 0.0 79516 3752 pts/1 S+ 10:31 0:00 sudo apt install -y python3 python3-pip
    usernam+ 9456 0.0 0.0 38892 944 pts/0 S+ 10:32 0:00 grep --color=auto -i apt

We need PID's (process IDs) from all lines except the last one. In our case it is `5183`.

Force kill the process by entering:

    sudo kill -9 5183

**Option 3**

If the previous options did not help you, or you do not have running `apt` and `apt-get` processes, run the following commands:

    sudo rm /var/lib/apt/lists/lock
    sudo rm /var/cache/apt/archives/lock
    sudo rm /var/lib/dpkg/lock
    sudo rm /var/lib/dpkg/lock-frontend
    sudo dpkg --configure -a

<pre style="background-color: #b7def5">When you run the program, you may receive messages about the inability  
to delete or the absence of files. Just ignore it.</pre>

After you complete these steps, the problem should be solve.

* * *

#### In the browser, instead of the site, a list of folders and files is visible.

Make sure you in the site folder before starting. It has a **.mkdocs** extention. Also, make sure that you have previously [created the site](https://pinderschlosse.gitlab.io/foliant_docs_en/quickstart/#create-and-run-a-local-site).
