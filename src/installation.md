# Installation
We recommend to use [Docker](https://www.docker.com/) because it does not require installation of Foliant and all its dependencies. If you want to use manual installation, it can be split on 3 stages:

- Installing Foliant with pip3
- Installing [Pandoc](https://pandoc.org/) and [TeXLive](http://www.tug.org/texlive/) bundles
- Installing backends

<pre style="background-color: LemonChiffon"><span>⚠</span> <strong>ATTENTION</strong>: You must have administrator privileges to successfully install  
all components.</pre>


## Install with Docker

**1. Installing Docker**

Select your operating system and follow the further instructions:

- [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [Windows](https://docs.docker.com/docker-for-windows/install/)
- [MacOS](https://docs.docker.com/docker-for-mac/install/)

>You can read about other installation options [here](https://docs.docker.com/). In the navigation menu (left), select: ***Get Docker >> Docker Engine-Community***.

**2. Installing the Foliant container**

    $ docker pull foliant/foliant

## Ubuntu

<pre style="background-color: LemonChiffon">To enter commands, use the terminal (Ctrl+Alt+T)</pre>

**1. Installing Python 3.6 and pip3**

- For versions **14.04** and **16.04**:
 
        $ sudo add-apt-repository ppa:jonathonf/python-3.6
        $ sudo apt update && sudo apt install -y python3 python3-pip

- For versions **18.04** and later:

        sudo apt update && sudo apt install -y python3 python3-pip

**2. Installing Foliant with pip3**

    $ sudo python3.6 -m pip3 install foliant foliantcontrib.init

**3. Installing additional Pandoc and TeXLive bundles**

    $ sudo apt install -y wget texlive-full librsvg2-bin
    $ sudo wget https://github.com/jgm/pandoc/releases/download/2.0.5/pandoc-2.0.5-1-amd64.deb &amp;&amp; sudo dpkg -i pandoc-2.0.5-1-amd64.deb

**4. Installing backends**

- **Mkdocs** (to generate web site):
 
        $ sudo pip3 install foliantcontrib.mkdocs

- **Pandoc** (to generate PDF, DOCX, TEX):
 
        $ sudo pip3 install foliantcontrib.pandoc

## Windows

<pre style="background-color: LemonChiffon"><span>⚠</span> <strong>ATTENTION</strong>: To enter commands, use the PowerShell CLI</pre> 

**Requirements**

+ Windows 64-bit (7 SP1, 8.1, Server 2008 R2 SP1, Server 2012, Server 2012 R2)
+ [PowerShell 5](https://www.microsoft.com/en-us/download/details.aspx?id=54616) or later ( including [PowerShell Core](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-6#installing-the-msi-package))
+ [.NET Framework 4.5](https://www.microsoft.com/ru-ru/search?q=.net+framework+4.5) or later
 
**1. Install Scoop with PowerShell**

    iex (new-object net.webclient).downloadstring('https://get.scoop.sh')

**2. Installing Python3 with Scoop**

    $ scoop install python

**3. Installing Foliant with pip**

    $ python -m pip install foliant foliantcontrib.init

**4. Installing Pandoc and MikTeX with Scoop**

    $ scoop install pandoc latex

## MacOS

<pre style="background-color: LemonChiffon">To enter commands, use the terminal (Command (⌘) + T)</pre>

**Requirements**

- [Homebrew](https://brew.sh/)

**1. Installing Python 3 with Homebrew**

    $ brew install python3

**2. Installing Foliant with pip3**

    $ python3 -m pip3 install foliant foliantcontrib.init

**3. Installing Pandoc and MacTeX with Homebrew**

    $ brew install pandoc mactex librsvg